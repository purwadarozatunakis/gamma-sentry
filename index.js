const express = require('express')
const app = express()
const port = 3000
const JsonDB = require('node-json-db');
var bodyParser = require('body-parser');
var uuid = require('uuid')

app.use(bodyParser.json())
// The second argument is used to tell the DB to save after each push
// If you put false, you'll have to call the save() method.
// The third argument is to ask JsonDB to save the database in an human readable format. (default false)
// The last argument is the separator. By default it's slash (/)
var db = new JsonDB("errors", true, false);
var dbdictonary = new JsonDB("dictonary", true, false);


app.get('/', (req, res) => res.send('Hello World!'))

app.post('/push/:app_key', (req, res) => {

    try {

        var report = req.body
        report.id = uuid.v1()
        report.timestamp = Math.floor(Date.now());

        db.push("/" + req.params.app_key + "/errors[]", report);


        res.send({
            status: 200,
            messages: "Data Reported"
        });

    } catch (error) {   

        res.send({
            status: 400,
            messages: "Data Not Reported"
        });

    }

})

app.get('/report/:app_key?', (req, res) => {
    if (req.params.app_key) {
        try {

            res.send({
                status: 200,
                datas: db.getData("/" + req.params.app_key)
            });
        } catch (error) {

            res.send({
                status: 400,
                messages: "Data Not Found"
            });
        }
    } else {

        res.send({
            status: 200,
            datas: db.getData("/")
        });
    }
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
